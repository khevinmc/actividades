create database DBActividades;
use DBActividades;

Create table Gerencia_central(
	id int not null primary key auto_increment unique,
    Nom_GeCe varchar(250)
);

Create table Area_solicitada(
	id int not null primary key auto_increment unique,
    Nom_Area varchar(250),
    id_GeCe int,
	foreign key (id_GeCe) REFERENCES Gerencia_central(id)
);

create table Persona_Solicitante(
	id int not null primary key auto_increment unique,
    nom_PerSol varchar(250),
    id_Are int,
	foreign key (id_Are) REFERENCES Area_solicitada(id)
);

Create table Modulo(
	id int not null primary key auto_increment unique,
    nom_Mod varchar(250)
);

Create table Usuario(
	id int not null primary key auto_increment unique,
    nom_Ape varchar(250),
    usuario varchar(250),
    contraseña varchar(250)
);

create table Actvidades(
	id int not null primary key auto_increment unique,
    fecha date,
    id_PerSol int,
    codigo_Ficha varchar(250),
    tipo_req varchar(250),
    detalle varchar(500),
    id_Mod int,
    id_Usuario int,
    estado varchar(250),
    programado varchar(250),
    transacion varchar(250),
    inicio_Des varchar(250),
    fin_Des varchar(250),
    horas_Des varchar(250),
    fecha_pase_pro date,
    status_final varchar(250),
	foreign key (id_PerSol) REFERENCES Persona_Solicitante(id),
	foreign key (id_Mod) REFERENCES Modulo(id),
    foreign key (id_Usuario) REFERENCES Usuario(id)
);

create table Proyectos(
	id int not null primary key auto_increment unique,
    empresa varchar(250),
    gerencia_central varchar(250),
    area_solicitante varchar(250),
    usuario_solicitante varchar(250),
    num_ficha varchar(250),
    tipo_requer varchar(250),
    sistema_aplicacion varchar(250),
    requerimiento varchar(3250),
    recurso varchar(250),
    estatus varchar(250),
    dias_hombre_plan varchar(250),
    cotizacion varchar(250),
    asignacion varchar(250),
    comentarios varchar(800)
);

Select * from Usuario;
Select * from Proyectos;
Select * from Actvidades;
Select tipo_requer from Proyectos group by tipo_requer;


insert into Proyectos(empresa,gerencia_central,area_solicitante,usuario_solicitante,num_ficha,tipo_requer,sistema_aplicacion,requerimiento,recurso,estatus,dias_hombre_plan,cotizacion,asignacion,comentarios)
values("AC FARMA","GG","GG","Javier Rubina","Solicitud Usuario","SAP","Proyecto WMS","Alcance:1.- Control de cambio para productos terminados.","EXTERNO","No tratada","20 semanas","70,271.00","OMNIA SOLUTION","");

