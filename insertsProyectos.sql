insert into Proyectos(empresa,gerencia_central,area_solicitante,usuario_solicitante,num_ficha,tipo_requer,sistema_aplicacion,requerimiento,recurso,estatus,dias_hombre_plan,cotizacion,asignacion,comentarios)
values("AC FARMA","GG","GG","Javier Rubina","Solicitud Usuario","SAP","Proyecto WMS","Alcance:1.- Control de cambio para productos terminados.","EXTERNO","No tratada","20 semanas","70,271.00","OMNIA SOLUTION","");

insert into Proyectos(empresa,gerencia_central,area_solicitante,usuario_solicitante,num_ficha,tipo_requer,sistema_aplicacion,requerimiento,recurso,estatus,dias_hombre_plan,cotizacion,asignacion,comentarios)
values("AC FARMA","GG","Directorio","Directorio","Solicitud Usuario","SAP","Evaluacion SAP S/4 HANA","Alcance:1.- Upgrade SAP","EXTERNO","No tratada","","","","");

insert into Proyectos(empresa,gerencia_central,area_solicitante,usuario_solicitante,num_ficha,tipo_requer,sistema_aplicacion,requerimiento,recurso,estatus,dias_hombre_plan,cotizacion,asignacion,comentarios)
values("AC FARMA","GOP","LOG","Jorge Colmenares","Solicitud Usuario","SAP","Capacidad de Planta - SAP","Alcance:Falta Completar","EXTERNO","No tratada","4 Semanas","9,980.00","OMNIA SOLUTION","");

insert into Proyectos(empresa,gerencia_central,area_solicitante,usuario_solicitante,num_ficha,tipo_requer,sistema_aplicacion,requerimiento,recurso,estatus,dias_hombre_plan,cotizacion,asignacion,comentarios)
values("GRUPO AC","GOP","Directorio","Directorio","Solicitud Usuario","SAP","Roll Out - Sagitario / Lacfarma","Alcance:1.- Implementacion del backoffice SAP para ambas compañias","EXTERNO","No tratada","16 semanas","148,680.00","OMNIA SOLUTION","");

insert into Proyectos(empresa,gerencia_central,area_solicitante,usuario_solicitante,num_ficha,tipo_requer,sistema_aplicacion,requerimiento,recurso,estatus,dias_hombre_plan,cotizacion,asignacion,comentarios)
values("AC FARMA","GOP","Mantenimiento","Max Huaman","Solicitud Usuario","SAP","Gestion de Mantenimiento PM","Alcance:Falta Completar","EXTERNO","No tratada","2.5 Semanas","26,914.68","SYSMANT");

insert into Proyectos(empresa,gerencia_central,area_solicitante,usuario_solicitante,num_ficha,tipo_requer,sistema_aplicacion,requerimiento,recurso,estatus,dias_hombre_plan,cotizacion,asignacion,comentarios)
values("AC FARMA","GOP","Direccion Tecnica","Roberto Chang","Solicitud Usuario","SAP","Administracion del ciclo de vida del Producto PLM","Alcance:Falta Completar","EXTERNO","No tratada","Sin Estimacion","","","");

insert into Proyectos(empresa,gerencia_central,area_solicitante,usuario_solicitante,num_ficha,tipo_requer,sistema_aplicacion,requerimiento,recurso,estatus,dias_hombre_plan,cotizacion,asignacion,comentarios)
values("AC FARMA","GCAF","FIN","Felipe Gajardo","Propuesta Interna","No SAP","Cartas Fianza","Alcance:1.- Documento se encuentra en manos de gestion pendiente de aprobacion.","IINHOUSE","No tratada","Sin Estimacion","","","");

insert into Proyectos(empresa,gerencia_central,area_solicitante,usuario_solicitante,num_ficha,tipo_requer,sistema_aplicacion,requerimiento,recurso,estatus,dias_hombre_plan,cotizacion,asignacion,comentarios)
values("AC FARMA","GCAF","CONT","Mc Callum","Propuesta Interna","No SAP","Liquidacion de gastos","Alcance:1.- Documento se encuentra en manos de gestion pendiente de aprobacion.","EXTERNA","No tratada","","","","");

insert into Proyectos(empresa,gerencia_central,area_solicitante,usuario_solicitante,num_ficha,tipo_requer,sistema_aplicacion,requerimiento,recurso,estatus,dias_hombre_plan,cotizacion,asignacion,comentarios)
values("AC FARMA","GCAF","RRHH","William Villena","Propuesta Interna","No SAP","Modulo de Control de Visitas","Alcance:1.- Implementar flujo de aprobacion de visitas. 2.- Captura de foto de visitante. 3.- Implementacion de blacklist de visitantes","IINHOUSE","No tratada","Sin Estimacion","","","");

insert into Proyectos(empresa,gerencia_central,area_solicitante,usuario_solicitante,num_ficha,tipo_requer,sistema_aplicacion,requerimiento,recurso,estatus,dias_hombre_plan,cotizacion,asignacion,comentarios)
values("AC FARMA","GG","Comex","Brenda Solari","Propuesta Interna","No SAP","Sistema COMEX","Alcance:1.- Documento se encuentra en manos de gestion pendiente de aprobacion.","INHOUSE","No tratada","Sin Estimacion","","","");

insert into Proyectos(empresa,gerencia_central,area_solicitante,usuario_solicitante,num_ficha,tipo_requer,sistema_aplicacion,requerimiento,recurso,estatus,dias_hombre_plan,cotizacion,asignacion,comentarios)
values("AC FARMA","GDMV","GDMV","Jorge Arriola","Propuesta Interna","No SAP","Registro Sanitario","Alcance:1.- Documento se encuentra en manos de gestion pendiente de aprobacion.","INHOUSE","No tratada","Sin Estimacion","","","");

insert into Proyectos(empresa,gerencia_central,area_solicitante,usuario_solicitante,num_ficha,tipo_requer,sistema_aplicacion,requerimiento,recurso,estatus,dias_hombre_plan,cotizacion,asignacion,comentarios)
values("AC FARMA","GDMV","Farmacovigilancia","Jose Pro","Solicitud Usuario","No SAP","Aplicación que administre y controle la informacion de la farmacovigilancia - EXTERNA","Alcance:1.- Automatizar el resguardo de informacion de farmacovigilancia externa","INHOUSE","No tratada","3 Semanas","","Ronald Carhuaricra","");

insert into Proyectos(empresa,gerencia_central,area_solicitante,usuario_solicitante,num_ficha,tipo_requer,sistema_aplicacion,requerimiento,recurso,estatus,dias_hombre_plan,cotizacion,asignacion,comentarios)
values("AC FARMA","GCC","Efectividad Com SP","Jorge Corrales","Solicitud Usuario","No SAP","Requerimientos SisDist - Jose Corrales","Alcance:1.- Ampliar caracteristicas de produccto: Productos Key2.- Estructura de cuotas 3.- Elaboracion de Reportes","INHOUSE","No tratada","Sin Estimacion","","Salomon Memenza","");

insert into Proyectos(empresa,gerencia_central,area_solicitante,usuario_solicitante,num_ficha,tipo_requer,sistema_aplicacion,requerimiento,recurso,estatus,dias_hombre_plan,cotizacion,asignacion,comentarios)
values("ECONOFARMA","GG","GG","Javier Rubina","Solicitud Usuario","No SAP","Abastecimiento Econofarma","Alcance:1.- Generar automáticamente la Propuesta de abastecimiento de PT 2.- Confirmar cantidad de PT y  elegir proveedor 3.- Generar automáticamente el pedido 4.- Calculo de Maximos y Minimos para la propuesta de abastecimiento ","INHOUSE","No tratada","Sin Estimacion","","Salomon Memenza","");

insert into Proyectos(empresa,gerencia_central,area_solicitante,usuario_solicitante,num_ficha,tipo_requer,sistema_aplicacion,requerimiento,recurso,estatus,dias_hombre_plan,cotizacion,asignacion,comentarios)
values("AC FARMA","GG","GG","Javier Rubina","Solicitud Usuario","No SAP","ServiceDesk Implementación de la Mesa de Ayuda de TI Invgate","Alcance:1.- Automatizacion de Requerimiento e incidencias (1era etapa: flujo TI)","EXTERNA","No tratada","","","");

insert into Proyectos(empresa,gerencia_central,area_solicitante,usuario_solicitante,num_ficha,tipo_requer,sistema_aplicacion,requerimiento,recurso,estatus,dias_hombre_plan,cotizacion,asignacion,comentarios)
values("AC FARMA","GCAF","RRHH","William Villena","Solicitud Usuario","No SAP","Automatizacion de entrega de boletas","Alcance:1.- Convertir boletas en formato PDF. 2.- Enviar boletas a cuentas personales de los colaboradores. 3.- hacer tracking de envio y recepcion de las boletas","INHOUSE","No tratada","4 Semanas","","Ronald Carhuaricra","");

insert into Proyectos(empresa,gerencia_central,area_solicitante,usuario_solicitante,num_ficha,tipo_requer,sistema_aplicacion,requerimiento,recurso,estatus,dias_hombre_plan,cotizacion,asignacion,comentarios)
values("AC FARMA","GCAF","RRHH","William Villena","Solicitud Usuario","No SAP","Solicitud de taxis","Alcance: Falta Completar","INHOUSE","No tratada","Sin Estimacion","","","");

insert into Proyectos(empresa,gerencia_central,area_solicitante,usuario_solicitante,num_ficha,tipo_requer,sistema_aplicacion,requerimiento,recurso,estatus,dias_hombre_plan,cotizacion,asignacion,comentarios)
values("AC FARMA","GCAF","RRHH","William Villena","Solicitud Usuario","No SAP","Modulo de Reconocimientos y beneficios RRHH	","Alcance:1.- Flujo de sugerencias y aprobaciones de reconocimientos a colaboradores. 2.- Implementar cartilla de premiaciones con acumulacion de puntajes.","INHOUSE","No tratada","Sin Estimacion","","Ronald Carhuaricra","");

insert into Proyectos(empresa,gerencia_central,area_solicitante,usuario_solicitante,num_ficha,tipo_requer,sistema_aplicacion,requerimiento,recurso,estatus,dias_hombre_plan,cotizacion,asignacion,comentarios)
values("AC FARMA","GG","Comex","Brenda Solari","Solicitud Usuario","No SAP","Integración SAP con SW de Agentes Aduaneros - COMEX","Alcance:1.-  Creacion de web service y rfc para la extraccion de la O/C . 2.- Lecturar los datos de costos del agente para ser insertados en el ambiente contable de SAP AC.","INHOUSE","No tratada","Sin Estimacion","","Salomon Memenza","");

insert into Proyectos(empresa,gerencia_central,area_solicitante,usuario_solicitante,num_ficha,tipo_requer,sistema_aplicacion,requerimiento,recurso,estatus,dias_hombre_plan,cotizacion,asignacion,comentarios)
values("AC FARMA","GOP","LOG","Jorge Colmenares","Solicitud Usuario","No SAP","Software de Demanda","Alcance:Falta Completar","EXTERNA","No tratada","2 semanas","25,000.00","IOSA - FORECAST PRO","");

insert into Proyectos(empresa,gerencia_central,area_solicitante,usuario_solicitante,num_ficha,tipo_requer,sistema_aplicacion,requerimiento,recurso,estatus,dias_hombre_plan,cotizacion,asignacion,comentarios)
values("AC FARMA","GG","Directorio","Directorio","Solicitud Usuario","No SAP","LIMS","Alcance:1.- Gestión de las especificaciones y características a controlar
2.- Gestión de las muestras
3.- Toma de muestras
4.- Introducción de resultados de los análisis
5.-Generación de avisos o incidencias
6.- Aprobación y cierre de resultados
7.- Integración con sistema de control (SCADAs, DCS)
8.- Integración con sistemas MES o ERP
9.- Gestión del estado de calibración de los instrumentos
10.- Informes con los resultados de las análisis y los límites de su especificación
11.- Información gráfica con valores reales y límites de las características críticas
12.- Trazabilidad de todos los eventos relacionados con la muestra","EXTERNA","No tratada","","","","");

insert into Proyectos(empresa,gerencia_central,area_solicitante,usuario_solicitante,num_ficha,tipo_requer,sistema_aplicacion,requerimiento,recurso,estatus,dias_hombre_plan,cotizacion,asignacion,comentarios)
values("AC FARMA","GCAF","RRHH","William Villena","Solicitud Usuario","No SAP","Ampliacion Jerarquica en Intranet","Alcance: 1.- utilizar el concepto de Dpto que maneja la actual version de OFISIS","INHOUSE","No tratada","3 semanas","","Salomon Memenza","");
