<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta http-equiv="X-UA-Compatible" content="ie=edge">
		<link href="<?php echo base_url(); ?>public/css/bootstrapAC.css" rel="stylesheet" />
		<link href="<?php echo base_url(); ?>public/css/user.css" rel="stylesheet" />
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
		<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
			integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous">
		</script>
		<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
			integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
		</script>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
			integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous">
		</script>
		<title>Document</title>
		<style>
		</style>
	</head>
	<body>
		<div class="container-fluid p-0">
			
			
			
			<div class="container-fluid d-flex justify-content-center bg-primary text-white">
				<h2>Actividades</h2>
			</div>
			
			<div class="nav container-fluid bg-dark d-flex justify-content-center pt-3 pb-3">
				
						
							<a id="acfiltro" style="text-decoration:none"class="p-2 mr-2 ml-2 bg-info text-white" href="#">Ac Farma</a>
							<a id="ecofiltro" style="text-decoration:none"class="p-2 mr-2 ml-2 bg-info text-white"href="#">Grupo Ac</a>
							<a id="grupofiltro" style="text-decoration:none"class="p-2 mr-2 ml-2 bg-info text-white"href="#">Econofarma</a>
						

						
					
			</div>
			
			<section>
				<div class="container-sm">
					
					<div class="container mt-3 ">
						<div class="row d-flex justify-content-center">
							<?php foreach ($proyectos as $key => $item):
							// var_dump($item -> requerimiento);exit;?>
							<div class="item col-sm-2 p-0 m-2  click-modal" data-listModal="<?php echo $key ?>">
								<a href="#" style="text-decoration:none" type="submit" class="asis col-12 text-white border d-flex justify-content-center align-items-center" data-toggle="modal"
									data-target="#myModal<?php echo $key ?>"
									 data-empresa-type="<?php echo $item->empresa; ?>"
									>
									<!-- <img class=""
									src="https://img.icons8.com/ios-glyphs/30/000000/today-apps.png"> -->
									<p><?php echo $item->sistema_aplicacion ?></p>
									
								</a>
							</div>
							<div class="modal fade" id="myModal<?php echo $key ?>">
								<div class="modal-dialog modal-lg modal-dialog-centered">
									<div class="modal-content" id="modalcss">
										<!-- Modal Header -->
										<div  class="modal-header border  bg-dark text-white">
											<p class=""><img
												src="https://img.icons8.com/ios-glyphs/30/000000/today-apps.png"><?php echo $item->sistema_aplicacion ?>
											</p>
										</div>
										<!-- Modal body -->
										<div class="container ">
											<div class="row border p-3">
												<div class="col-12 ">
													<p><?php echo $item->requerimiento ?></p>
												</div>
											</div>
											<div  class="row border ">
												<div  class="col-4 p-3 border">
													<p><?php echo $item->estatus ?></p>
												</div>
												<div class="col-4 p-3 border">
													<p><?php echo $item->dias_hombre_plan ?></p>
												</div>
												<div  class="col-4 p-3 border">
													<p>S/. <?php echo $item->cotizacion ?> </p>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<?php endforeach;?>
						</div>
					</div>
				</div>
			</div>
		</section>
		<script src="<?php echo base_url(); ?>public/js/user.js"></script>
	</body>
</html>