<!DOCTYPE html>
<html lang="es">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<link href="<?php echo base_url();?>public/css/bootstrap.css" rel="stylesheet" />


	<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
		integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous">
	</script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
		integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
	</script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
		integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous">
	</script>
	<style>
		html {
			height: 100%;
			width: 100%;
		}

		#section {
			height: 80%;
			width: 100%;
		}

		#modalcss {
			border-radius: 40px;
		}

		#modalh {
			border-radius: 40px 40px 0 0;
		}

	</style>

	<title>Document</title>
</head>

<body>



	<div id="section" class="col-xl-12 d-flex justify-content-center align-items-center" id="ab">
		<form action="<?php echo base_url()?>Login/iniciar_session_post" method="POST" class="px-4 py-3">

			<div class="form-group">
				<label for="exampleDropdownFormEmail2" id="user">Usuario</label>
				<input name="usuario" type="text" class="form-control" id="exampleDropdownFormEmail2">
			</div>
			<div class="form-group">
				<label for="exampleDropdownFormPassword2" id="contra">Contraseña</label>
				<input name="pass" type="password" class="form-control" id="exampleDropdownFormPassword2">
			</div>
			<div class="form-group">
				<div class="form-check">

					<input type="checkbox" class="form-check-input" id="dropdownCheck2">
					<label class="form-check-label" id="re" for="dropdownCheck2">
						Recuerdame
					</label>
					<?php if ($error): ?>
					<p class="text-danger"> <?php echo $error ?> </p>
					<?php endif; ?>
				</div>
			</div>
			<button id="ingresar" type="submit" class="btn btn-primary">Ingresar</button>
			<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
				Registrar
			</button>

			<!-- The Modal -->


		</form>

	</div>
	<div class="modal fade" id="myModal">
		<div class="modal-dialog">
			<div class="modal-content" id="modalcss">

				<!-- Modal Header -->
				<div id="modalh" class="modal-header d-flex justify-content-center">
					<h4 class="modal-title ">Registro de Usuario</h4>

				</div>

				<!-- Modal body -->
				<div id="section" class="col-xl-12 d-flex justify-content-center align-items-center" id="ab">
					<form action="<?php echo base_url()?>Login/register" method="POST" class="px-4 py-3">

						<div class="form-group">
							<label for="exampleDropdownFormEmail2" id="userR">Usuario:</label>
							<input name="usuarioR" type="text" class="form-control">
						</div>
						<div class="form-group">
							<label for="exampleDropdownFormPassword2" id="contrR">Contraseña:</label>
							<input name="passR" type="password" class="form-control">
						</div>
						<div class="form-group">
							<label for="exampleDropdownFormEmail2" id="nombreR">Nombres:</label>
							<input name="nombreR" type="text" class="form-control">
						</div>
						<div class="form-group">
							<label for="exampleDropdownFormEmail2" id="apellidoR">Apellidos:</label>
							<input name="apellidoR" type="text" class="form-control">
						</div>
						<div class="form-group">
							<label for="exampleDropdownFormEmail2" id="correoR">Correo:</label>
							<input name="correoR" type="text" class="form-control">
						</div>
						<div class="form-group d-flex justify-content-center">
							<button id="registrar" type="submit"
								class="btn btn-primary justify-content-center">Registrar</button>

						</div>


					</form>

				</div>

			</div>
		</div>
	</div>


	</script>
</body>

</html>
