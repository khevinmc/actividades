
<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Login_Model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function usuario_nombre($nombre, $pass)
    {
        $this->db->select('id,usuario,contraseña, nom_Ape');
        $this->db->from('Usuario');
        $this->db->where('usuario', $nombre);
        $this->db->where('contraseña', $pass);
        $consulta = $this->db->get();
        $resultado = $consulta->row();
        return $resultado;
    }

    public function registrar_usuario()
    {
        $data = array(
            'usuario' => $this->input->post('usuarioR'),
            'contraseña' => $this->input->post('passR'),
            'nom_Ape' => $this->input->post('nombreR'),
        );
        return $this->db->insert('Usuario', $data);
        redirect(base_url() . 'user');

    }

    public function mostrar_usuarios()
    {
        $this->db->select('*');
        $this->db->from('Proyectos');
        $consulta = $this->db->get();
        $resultado = $consulta->result();
        return $resultado;
    }

	public function eliminar_usuarios($id)
	{
		$this->db->delete();
		$this->db->from('Usuario');
		$this->db->where('');
	}
	public function getUsernames(){

		$this->db->select('nom_Ape');
		$this->db->from('Usuario');
		$records = $this->db->get();
		$users = $records->result();
		return $users;
		// var_dump( $records->result_array());exit;
	  }
	  public function getUserDetails($postData=array()){
	 
		$response = array();
	 
		if(isset($postData['requerimiento']) ){
	 
		  // Select record
		  $this->db->select('*');
		  $this->db->where('requerimiento', $postData['requerimiento']);
		  $records = $this->db->get('Proyectos');
		  $response = $records->result_array();
	 
		}
	 
		return $response;
	  }
	
}
