<?php
if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Login extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->library('session');
		$this->load->model('Login_Model');
		$this->load->helper('url');
	}

	public function iniciar_sesion() {
		$data = array();
		$data['error'] = $this->session->flashdata('error');
		$this->load->view('templates/header');
		$this->load->view('login_view', $data);
		$this->load->view('templates/footer');
	}

	public function iniciar_session_post() {
		if ($this->input->post()) {
			$nombre = $this->input->post('usuario');
			$pass = $this->input->post('pass');
			$usuario1 = $this->Login_Model->usuario_nombre($nombre, $pass);
			$usuario2 = $this->session->userdata("usuario");
			if ($usuario1) {
				$usuario_data = array(
					'usuario' => $nombre,
					'contraseña' => $pass,
					'nom_Ape' => $usuario1->nom_Ape,
					'logueado' => true,
				);
				$this->session->set_userdata($usuario_data);
				redirect(base_url("User"));
			} else {
				$this->session->set_flashdata('error', 'El usuario o la contraseña son incorrectos.');
				redirect(base_url());
			}
		} else {
			$this->iniciar_sesion();
		}
	}

	public function cerrar_sesion() {
		$usuario_data = array(
			'logueado' => false,
		);
		$this->session->set_userdata($usuario_data);
		redirect(base_url());
	}

	public function register() {
		$this->load->model('Login_Model');
		$this->Login_Model->registrar_usuario();
		redirect(base_url() . 'user');
	}
}
